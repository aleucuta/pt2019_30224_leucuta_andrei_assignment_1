package application;

import javax.swing.JFrame;

import view.MainView;

public class Application_1 {
	
	public static void main(String[] args) {
		JFrame view = new MainView("Calculator");
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.setVisible(true);
	}
}
