package view;

import javax.swing.JFrame;

import controller.Controller;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

public class MainView extends JFrame{

	private static final long serialVersionUID = 1L;
	
	private JTextField firstPolynomField;
	private JTextField secondPolynomField;
	private JTextField resultPolynomField;
	private JButton addButton;
	private JButton substractButton;
	private JButton multiplyButton;
	private JButton divideButton;
	private JButton integrateButton;
	private JButton derivateButton;
	private JLabel title;
	private JPanel boxPanel;
	private JPanel textPanel;
	
	Controller controller = new Controller(this);
	
	
	public MainView(String name) {
		super(name);
		getContentPane().setLayout(new BorderLayout(50,50));
		
		this.setBounds(0,  0,  640,  480);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		addButton = new JButton("+");
		addButton.setFont(new Font("TimesRoman",Font.BOLD,24));
		addButton.addActionListener(controller);

		
		substractButton = new JButton("-");
		substractButton.setFont(new Font("TimesRoman",Font.BOLD,24));
		substractButton.addActionListener(controller);

		
		multiplyButton = new JButton("x");
		multiplyButton.setFont(new Font("TimesRoman",Font.BOLD,24));
		multiplyButton.addActionListener(controller);

		
		divideButton = new JButton("/");
		divideButton.setFont(new Font("TimesRoman",Font.BOLD,24));
		divideButton.addActionListener(controller);

		
		integrateButton = new JButton("∫");
		integrateButton.setFont(new Font("Arial",Font.BOLD,24));
		integrateButton.addActionListener(controller);

		
		derivateButton = new JButton("∂");
		derivateButton.setFont(new Font("Arial",Font.BOLD,24));
		derivateButton.addActionListener(controller);

		
		boxPanel = new JPanel();
		boxPanel.setLayout(new GridLayout(6,2));
		boxPanel.setPreferredSize(new Dimension(100,400));
		
		
		boxPanel.add(addButton);
		boxPanel.add(Box.createRigidArea(new Dimension(0,10)));
		boxPanel.add(substractButton);
		boxPanel.add(Box.createRigidArea(new Dimension(0,10)));
		boxPanel.add(multiplyButton);
		boxPanel.add(Box.createRigidArea(new Dimension(0,10)));
		boxPanel.add(divideButton);
		boxPanel.add(Box.createRigidArea(new Dimension(0,10)));
		boxPanel.add(integrateButton);
		boxPanel.add(Box.createRigidArea(new Dimension(0,10)));
		boxPanel.add(derivateButton);
		getContentPane().add(boxPanel, BorderLayout.EAST);
		
		title = new JLabel("Polynoms Calculator");
		title.setFont(new Font("TimesRoman",Font.BOLD,30));
		title.setHorizontalAlignment(JLabel.CENTER);
		
		firstPolynomField = new JTextField("10.42x^3 + 12.2x ^2 + 3.0"); 
		secondPolynomField = new JTextField("4x^5 + 1");
		resultPolynomField = new JTextField();
		JLabel firstPolynomLabel = new JLabel("First Polynom:");
		firstPolynomLabel.setFont(new Font("TimesRoman",Font.PLAIN,24));
	
		JLabel secondPolynomLabel = new JLabel("Second Polynom:");
		secondPolynomLabel.setFont(new Font("TimesRoman",Font.PLAIN,24));
		
		JLabel resultPolynomLabel = new JLabel("Result");
		resultPolynomLabel.setFont(new Font("TimesRoman",Font.PLAIN,24));
		
		textPanel = new JPanel();
		textPanel.setLayout(new GridLayout(7,1));
		
		textPanel.add(firstPolynomLabel);
		textPanel.add(firstPolynomField);
		textPanel.add(secondPolynomLabel);
		textPanel.add(secondPolynomField);
		textPanel.add(Box.createRigidArea(new Dimension(0,10)));
		textPanel.add(resultPolynomLabel);
		textPanel.add(resultPolynomField);
		
		JPanel emptyPanel = new JPanel();
		JPanel emptyPanel2 = new JPanel();
		
	getContentPane().add(emptyPanel, BorderLayout.SOUTH);
		getContentPane().add(emptyPanel2, BorderLayout.WEST);
		getContentPane().add(textPanel, BorderLayout.CENTER);
		getContentPane().add(title, BorderLayout.NORTH);
		
	}
	
	public String getFirstPolynomFieldText() {
		return this.firstPolynomField.getText();
	}
	
	public String getSecondPolynomFieldText() {
		return this.secondPolynomField.getText();
	}
	
	public void setResultPolynomFieldText(String polynom) {
		this.resultPolynomField.setText(polynom);
	}
		
}
