package logic;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import model.Polynom;
import model.Term;

public class Operations {

	public static Map<String, BiConsumer<Polynom, Polynom>> operationMap = new HashMap<String, BiConsumer<Polynom, Polynom>>();
	public static Polynom result;
	public static Polynom remainder;

	public static void init() {
		try {
			operationMap.put("+", (p1, p2) -> addPolynoms(p1, p2));
			operationMap.put("-", (p1, p2) -> substractPolynoms(p1, p2));
			operationMap.put("x", (p1, p2) -> multiplyPolynoms(p1, p2));
			operationMap.put("/", (p1, p2) -> dividePolynoms(p1, p2));
			operationMap.put("∫", (p1, p2) -> integratePolynoms(p1, p2));
			operationMap.put("∂", (p1, p2) -> derivatePolynoms(p1, p2));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void addPolynoms(Polynom firstPolynom, Polynom secondPolynom) {
		result = new Polynom();
		result.copyPolynom(firstPolynom);
		for (Term each : secondPolynom.getTerms()) {
			result.addTerm(each);
		}
		remainder = null;
	}

	public static void substractPolynoms(Polynom firstPolynom, Polynom secondPolynom) {
		result = new Polynom();
		result.copyPolynom(firstPolynom);
		for (Term each : secondPolynom.getTerms()) {
			result.addTerm(new Term(-each.getCoef(), each.getDegree()));
		}
		remainder = null;
	}

	public static void multiplyPolynoms(Polynom firstPolynom, Polynom secondPolynom) {
		result = new Polynom();
		for (Term each : firstPolynom.getTerms()) {
			for (Term second : secondPolynom.getTerms()) {
				Term product = each.multiply(second);
				result.addTerm(product);
			}
		}
		remainder = null;
		//result = new Polynom(resultPolynom.getTerms());
	}

	public static void dividePolynoms(Polynom firstPolynom, Polynom secondPolynom) {
		Polynom aux = new Polynom();
		firstPolynom.sortPolynom();
		secondPolynom.sortPolynom();
		if(firstPolynom.getDegree() < secondPolynom.getDegree()) {
			remainder = firstPolynom;
			result = new Polynom("0");
			return;
		}
		Polynom bigResult = new Polynom();
		while(firstPolynom.getDegree() >= secondPolynom.getDegree()) {
			
			Term firstDegDiv = firstPolynom.getTerms().get(0).divide(secondPolynom.getTerms().get(0));
			bigResult.addTerm(firstDegDiv);
			for (Term each:secondPolynom.getTerms()) {
				aux.addTerm(firstDegDiv.multiply(each));
			}
			substractPolynoms(firstPolynom,aux);
			aux = new Polynom();
			firstPolynom = result;
		}
		remainder = result;
		result = bigResult;
	}

	public static void derivatePolynoms(Polynom firstPolynom, Polynom secondPolynom) {
		// second polynom is just a dummy polynom
		result = new Polynom();
		for (Term each : firstPolynom.getTerms()) {
			if (each.getDegree() > 0)
				result.addTerm(new Term(each.getCoef() * each.getDegree(), each.getDegree() - 1));
		}
		remainder = null;
	}
	
	public static void integratePolynoms(Polynom firstPolynom, Polynom secondPolynom) {
		// second polynom is just a dummy
		result = new Polynom();
		for(Term each:firstPolynom.getTerms()) {
			if(each.getCoef() != 0)
				result.addTerm(new Term(each.getCoef()/(each.getDegree()+1),each.getDegree()+1));
		}
		remainder = null;
	}
}
