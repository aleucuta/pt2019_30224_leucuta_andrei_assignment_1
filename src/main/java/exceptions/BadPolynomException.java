package exceptions;

public class BadPolynomException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadPolynomException(String message) {
		super(message);
	}
}
