package model;

public class Term implements Comparable<Term>{
	private float coef;
	private int degree;

	public Term(float coef, int degree) {
		this.coef = coef;
		this.degree = degree;
	}

	@Override
	public String toString() {
		char sign = '+';
		if(coef < 0)
			sign = '-';
		if(this.degree == 1)
			return sign + " " + String.valueOf(Math.abs(this.coef)) + "x" + " ";
		if (this.degree > 0)
			return sign + " " + String.valueOf(Math.abs(this.coef)) + "x^" + Integer.toString(this.degree) + " ";
		else
			return sign + " " + String.valueOf(Math.abs(this.coef));
	}

	public Term multiply(Term term) {

		return new Term(this.coef * term.coef, this.degree + term.degree);
	}
	
	public Term divide(Term term) {
		return new Term(this.coef / term.coef, this.degree - term.degree);
	}

	public float getCoef() {
		return coef;
	}

	public void setCoef(float coef) {
		this.coef = coef;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public int compareTo(Term arg0) {
		if(arg0.degree == this.degree)
			return (int) (arg0.coef - this.coef);
		return arg0.degree - this.degree;
	}

}
