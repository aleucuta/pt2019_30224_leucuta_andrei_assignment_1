package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import controller.Controller;

public class Polynom {
	List<Term> terms;
	
	public Polynom() {
		this.terms = new ArrayList<Term>();
	}
	
	public Polynom(String polynomString) {
		this.terms = Controller.getPolynomFromText(polynomString).getTerms();
	}
	
	public Polynom(List<Term> terms) {
		this.terms = terms;
	}
	
	public int getDegree() {
		
		Collections.sort(this.terms);
		return terms.get(0).getDegree();
	}
	
	public void copyPolynom(Polynom original) {
		Polynom copy = new Polynom();
		for(Term each:original.getTerms()) {
			
			copy.addTerm(each);
		}
		this.terms = copy.getTerms();
	}
	
	public void addTerm(Term newTerm) {
		Term aux = null;
		for (Term each : this.terms) {
			if (each.getDegree() == newTerm.getDegree()) {
				if(each.getCoef()+newTerm.getCoef() != 0)
				{
					each.setCoef(each.getCoef() + newTerm.getCoef());
					return;
				}
				else
					aux = each;
			}
		}
		if(aux != null) {
			this.terms.remove(aux);
			return;
		}
		this.terms.add(new Term(newTerm.getCoef(),newTerm.getDegree()));
		return;
	}
	
	public void sortPolynom() {
		Collections.sort(this.terms);
	}
	
	@Override
	public String toString() {
		this.sortPolynom();
		StringBuilder str = new StringBuilder();
		
		for(Term each : this.terms) {
			if(each == this.terms.get(0) && each.getCoef()>0) {
					str.append((each.toString()).substring(2));
			}
			else
				str.append(each);
		}
		return str.toString();
	}
	
	public List<Term> getTerms(){
		return this.terms;
	}
	
	
}
