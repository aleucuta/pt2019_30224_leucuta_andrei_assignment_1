package controller;

import logic.Operations;
import model.Polynom;
import model.Term;
import view.MainView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import exceptions.BadPolynomException;

public class Controller implements ActionListener {

	private MainView mainView;

	public Controller(MainView mainView) {
		this.mainView = mainView;
		this.mainView.setVisible(true);

	}

	public static Polynom getPolynomFromText(String text) throws BadPolynomException {

		Polynom polynom = new Polynom();
		text = text.replaceAll("\\s+", "");

		List<String> terms = getTerms(text);
		for (String each : terms) {
			Float coef = getCoef(each);
			Integer power;
			power = getPower(each);
			Term nTerm = new Term(coef, power);
			polynom.addTerm(nTerm);
		}

		return polynom;
	}

	private static List<String> getTerms(String polynomText) throws BadPolynomException {
		List<String> terms = new ArrayList<String>();

		Pattern regexPattern = Pattern
				.compile("([+-]?(?:(?:\\d*\\.?\\d*[xX]\\^\\d+)|(?:\\d+\\.?\\d*[xX])|(?:\\d+\\.?\\d*)|(?:[xX])))");
		Matcher match = regexPattern.matcher(polynomText);
		while (match.find()) {
			terms.add(match.group(1));
		}

		if (terms.size() == 0) {

			throw new BadPolynomException("Bad polynom!");

		}
		return terms;

	}

	private static Float getCoef(String term) throws BadPolynomException {
		Pattern floatDigits = Pattern.compile("\\d+\\.?\\d*");
		Matcher coef = floatDigits.matcher(term);
		Float coeficient = (float) 0;
		if (term.matches("[+-]?[xX].*"))
			coeficient = (float) 1;
		else if (coef.find()) {
			coeficient = Float.valueOf(coef.group());
			if (term.matches("-.*"))
				coeficient *= -1;
		} else
			throw new BadPolynomException("Bad polynom!");
		return coeficient;

	}

	private static Integer getPower(String term) throws BadPolynomException {
		Integer termPower = 0;
		Pattern intNr = Pattern.compile("x\\^([0-9]+)$");
		if (term.matches(".*\\^-.*"))
			throw new BadPolynomException("Bad polynom!");
		Matcher power = intNr.matcher(term);
		if (term.matches("[+-]?\\d*\\.?\\d*[xX]")) {
			termPower = 1;
			return termPower;
		}

		if (power.find()) {
			termPower = Integer.parseInt(power.group(1));
		}

		return termPower;
	}

	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		String command = null;
		if (source instanceof JButton) {
			command = ((JButton) source).getText();
			Operations.init();
			try {
				String firstPolynomText = this.mainView.getFirstPolynomFieldText();
				String secondPolynomText = this.mainView.getSecondPolynomFieldText();

				Polynom firstPolynom = getPolynomFromText(firstPolynomText);
				Polynom secondPolynom = getPolynomFromText(secondPolynomText);
				Operations.operationMap.get(command).accept(firstPolynom, secondPolynom);
				if (Operations.remainder!= null) {
				//this.mainView.setResultPolynomFieldText("");
					this.mainView.setResultPolynomFieldText(Operations.result.toString() + ",rest: " + Operations.remainder.toString());
				}
				else
					this.mainView.setResultPolynomFieldText(Operations.result.toString());

				} catch (BadPolynomException e2) {
					this.mainView.setResultPolynomFieldText("Bad polynom!");
				}
		}
	}

}
