package logicTests;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import model.Polynom;
import logic.Operations;

public class LogicTests{
	
	@Test
	public void addPolynomsTest() {
		Polynom testPolynom = new Polynom("4x + 2");
		Polynom testPolynom2 = new Polynom("5x^2 + 4");
		Operations.addPolynoms(testPolynom,testPolynom2);
		assertTrue(Operations.result.toString().equals("5.0x^2 + 4.0x + 6.0"));
	}
	
	@Test
	public void substractPolynomsTest() {
		Polynom testPolynom2 = new Polynom("4x + 2");
		Polynom testPolynom = new Polynom("5x^2 + 4");
		Operations.substractPolynoms(testPolynom,testPolynom2);
		assertTrue(Operations.result.toString().equals("5.0x^2 - 4.0x + 2.0"));
	}
	@Test
	public void multiplyPolynomsTest() {
		Polynom testPolynom = new Polynom("4x + 2");
		Polynom testPolynom2 = new Polynom("5x^2 + 4");
		Operations.multiplyPolynoms(testPolynom,testPolynom2);
		assertTrue(Operations.result.toString().equals("20.0x^3 + 10.0x^2 + 16.0x + 8.0"));
	}
	
	@Test
	public void dividePolynomsTest() {
		Polynom testPolynom = new Polynom("4x^3 + 2x + 5");
		Polynom testPolynom2 = new Polynom("x+1");
		Operations.dividePolynoms(testPolynom,testPolynom2);
		assertTrue(Operations.result.toString().equals("4.0x^2 - 4.0x + 6.0"));
		assertTrue(Operations.remainder.toString().contentEquals("- 1.0"));
	}
	
	@Test
	public void derivatePolynomsTest() {
		Polynom testPolynom = new Polynom("4x^3 + 2x + 5");
		Polynom testPolynom2 = new Polynom("x+1");
		Operations.derivatePolynoms(testPolynom,testPolynom2);
		assertTrue(Operations.result.toString().equals("12.0x^2 + 2.0"));
	}
	
	@Test
	public void integratePolynomsTest() {
		Polynom testPolynom = new Polynom("4x^3 + 2x + 5");
		Polynom testPolynom2 = new Polynom("x+1");
		Operations.integratePolynoms(testPolynom,testPolynom2);
		System.out.println(Operations.result.toString());
		assertTrue(Operations.result.toString().equals("1.0x^4 + 1.0x^2 + 5.0x "));
	}
	
	
}