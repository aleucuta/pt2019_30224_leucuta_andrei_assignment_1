package modelTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import exceptions.BadPolynomException;
import model.Polynom;
import model.Term;

public class ModelTests{
	
	@Test
	public void termTest() {
		Term testTerm = new Term(5,2);
		String test = "+ 5.0x^2 ";
		assertTrue(testTerm.toString().equals(test));
		
		Term testTerm2 = new Term(4,2);
		test = "+ 20.0x^4 ";
		assertTrue(testTerm.multiply(testTerm2).toString().contentEquals(test));
		
		test = "+ 1.25";
		assertTrue(testTerm.divide(testTerm2).toString().equals(test));
	
		assertTrue(testTerm.compareTo(testTerm2) < 0);
	}
	
	@Test
	public void polynomTest() {
		Polynom testPolynom = new Polynom("2x^3 + 3x + 4");
		assertTrue(testPolynom.toString().equals("2.0x^3 + 3.0x + 4.0"));
		
		Polynom testPolynom2 = new Polynom(testPolynom.getTerms());
		assertTrue(testPolynom2.toString().equals("2.0x^3 + 3.0x + 4.0"));
		
		Polynom testPolynom3 = new Polynom();
		testPolynom3.copyPolynom(testPolynom2);
		assertTrue(testPolynom3.toString().equals("2.0x^3 + 3.0x + 4.0"));
		
		Term term = new Term(3,2);
		testPolynom.addTerm(term);
		term = new Term(4,0);
		testPolynom.addTerm(term);
		assertTrue(testPolynom.toString().equals("2.0x^3 + 3.0x^2 + 3.0x + 8.0"));
		//System.out.println(testPolynom.toString());
		
		try {
		testPolynom = new Polynom("asdfasdf");
		assertEquals(0,1);
		}catch(BadPolynomException e1) {
			assertEquals(1,1);
		}
	}
}